package music;

import java.util.Iterator;
import table.TableInterface;
import table.TableFactory;

public class Artist{
	private TableInterface<CD, String> cds;
	private String artist_name;
	
	public Artist(String name){
		 cds=TableFactory.createTable(new CompareCDTitles(true));
		artist_name=name;
	}
	public void addCD(CD cd){
		cds.tableInsert(cd);
	}
	public String getArtist(){
		return artist_name;
	}
	public String toString(){
      return artist_name;
    }
	public Iterator<CD> iterator(){
		return cds.iterator();
	}
}