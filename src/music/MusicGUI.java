package music;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.dnd.*;

import java.util.Iterator;

import gui.*;

public class MusicGUI extends CenterFrame implements Drawable {
	private JComboBox<Artist> artists;
	private JComboBox<CD> cds;
	private ManySongs manySongs;
	private SingleSong single_song;
	private ImageLoader imageLoader = ImageLoader.getImageLoader();
	private Image CD_Image;
	private Image IMG;
	
	public MusicGUI(int width, int height, Iterator<Artist> it) {
		super(width, height, "Music Collection");
		CD_Image = imageLoader.getImage("resources/art/Origin - Omnipresent.jpg");
		IMG = CD_Image;
		
		setLayout(new BorderLayout());
		JPanel center_grid = new JPanel();
		add(center_grid, BorderLayout.CENTER);
		center_grid.setBackground(Color.white);
		JPanel northPanel = new JPanel();
		add(northPanel, BorderLayout.NORTH);
		northPanel.setLayout(new GridLayout(1, 2));
		artists = new JComboBox();
		cds = new JComboBox();
		northPanel.add(artists);
		northPanel.add(cds);
		artists.setActionCommand("artists");
		ComboEventHandling ceh = new ComboEventHandling();
		
		manySongs = new ManySongs();
		manySongs.setFont(new Font("Courier New", Font.PLAIN, 12));
		manySongs.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		Artist art = new Artist("");
		
		while(it.hasNext()) {
			art = it.next();
			String a = art.getArtist();
			artists.addItem(art);
			
		}
		
		CD artistCD;
		Iterator<CD> iter2 = art.iterator();
		while(iter2.hasNext()) {
			artistCD = iter2.next();
			cds.addItem(artistCD);
		}
		
		Iterator<CD> iter3 = art.iterator();
		CD cd = iter3.next();
		manySongs.setCD(cd);
				
		artists.addActionListener(ceh);
		
		Dimension d = new Dimension(10, 17);
		
		JScrollPane scrollSongs = new JScrollPane();
		scrollSongs.setPreferredSize(d);
		scrollSongs.getViewport().add(manySongs);
		
		JPanel scrollPanel = new JPanel();
		scrollPanel.setLayout(new BorderLayout());
		scrollPanel.add(scrollSongs, BorderLayout.CENTER);
		
		gui.EasyGridBag egb = new gui.EasyGridBag(1, 4, center_grid);
		center_grid.setLayout(egb);
		
		DrawPanel dp = new DrawPanel();
		dp.setDrawable(this);
		
		egb.fillCellWithRowColSpan(1, 2, 1, 4, GridBagConstraints.BOTH, dp);
		egb.fillCellWithRowColSpan(1, 1, 1, 1, GridBagConstraints.BOTH, scrollPanel);
		
		single_song = new SingleSong();
		single_song.setPreferredSize(d);
		
		scrollPanel.add(single_song, BorderLayout.SOUTH);
		
		DragSource dragSource = DragSource.getDefaultDragSource();
		DragGestureRecognizer dgr = dragSource.createDefaultDragGestureRecognizer(manySongs, DnDConstants.ACTION_COPY, manySongs);
		DropTarget dropTarget = new DropTarget(single_song, single_song);
		
		setVisible(true);	
		
	}
	
	private class ComboEventHandling implements ActionListener {
		public void actionPerformed(ActionEvent evt) {
			if(evt.getActionCommand().equals("artists")) {
				cds.removeAllItems();
				Artist art = artists.getItemAt(artists.getSelectedIndex());
				Iterator<CD> it = art.iterator();
				while(it.hasNext()) {
					CD artistCD = it.next();
					cds.addItem(artistCD);
				}
				
				int selectedIndex = cds.getSelectedIndex();
				CD cd = cds.getItemAt(selectedIndex);
				manySongs.setCD(cd);
				String file = "resources/art/" + cd.getArtist() + " - " + cd.getTitle() + ".jpg";
				CD_Image = imageLoader.getImage(file);
				IMG = CD_Image;
				System.out.println("combo box event");
			}
			
		}
	}
	
	public void draw(Graphics g, int width, int height) {
		int selectedIndex = cds.getSelectedIndex();
		CD cd = cds.getItemAt(selectedIndex);
		
		g.drawImage(CD_Image, width - 300, height - 300, null);
		g.setColor(Color.black);
		Font font = new Font("Verdana", Font.BOLD, 16);
		g.setFont(font);
		g.drawString(cd.getArtist() + " - " + cd.getTitle(), 20, 20);
	}
	
	public void mouseClicked(int x, int y) {
		System.out.println("mouse clicked");
		if(x >= CD_Image.getWidth(null) - 38 && x <= CD_Image.getWidth(null) + 162 && y >= CD_Image.getHeight(null) - 85 && y <= CD_Image.getHeight(null) + 115) {
			CD_Image = imageLoader.getHighLightImage(CD_Image);
		} else {
			CD_Image = IMG;
		}
	}
	
	public void keyPressed(char key) {
		System.out.println("key pressed");
	}
}