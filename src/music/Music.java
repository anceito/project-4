package music;

import java.util.Iterator;
import table.TableInterface;
import table.TableFactory;
import gui.BasicGUI;

public class Music
{
   private TableInterface<Artist, String> artists;


   public Music(String file_name){
	   artists=TableFactory.createTable(new CompareArtists(true));
	   readMusic(file_name);
   }
   
   
   public Iterator<Artist> iterator(){
	   return artists.iterator();
   }
   
   
   private void readMusic(String file_name)
   {
      util.ReadTextFile rf = new util.ReadTextFile(file_name);
      String art = rf.readLine();
	  Artist isArtist;
      while (!rf.EOF())
      {
         String title = rf.readLine();
         System.out.println(title);
         int year = Integer.parseInt(rf.readLine());
         int rating = Integer.parseInt(rf.readLine());
         int numTracks = Integer.parseInt(rf.readLine());
         CD cd = new CD(title, art, year, rating, numTracks);

         int tracks = 1;

         while (tracks <= numTracks)
         {
            String temp = rf.readLine();
            String[] line = temp.split(",");
            String len = line[0];
            String song_title = line[1];
			Song song = new Song(song_title, len, art, cd.getTitle(), tracks);
            cd.addSong(song);
            tracks++;
         }
		 
		 //DO THIS
         //if the artist isn't already present in the table, create a new artist and insert it
		 
		Artist ar= artists.tableRetrieve(art);
		 if(ar==null){
			 ar = new Artist(art);
			 artists.tableInsert(ar);
		 }
		 ar.addCD(cd);
			
         
		 
		 
		 
		 
		 
		 

         art = rf.readLine();
      }
	  
	  rf.close();
   }

   public static void main(String[] args)
   {
	  
      Music mc = new Music("resources/cds.txt");
      //instantiate your GUI here
	 MusicGUI music_gui= new MusicGUI(640,480,mc.iterator());
	  
   }
}